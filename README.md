Carta de referencia o carta de recomendación

A la hora de buscar trabajo, las referencias personales o profesionales pueden ayudarle a reforzar su candidatura. Normalmente, estas referencias adoptan la forma de una carta de referencia, también conocida como carta de recomendación.

Estas cartas se utilizan para demostrar su experiencia pasada a un nuevo empleador. Si todavía no sabes qué son las cartas de referencia o cómo escribirlas si te las piden, te recomendamos que sigas leyendo.

¿Qué es una carta de referencia?
Una carta de referencia, también conocida como carta de recomendación, es un agradecimiento escrito que un antiguo colega o jefe escribe para usted. Está dirigido a tus posibles futuros empleadores y pretende detallar tus principales logros y puntos fuertes.

Tipos de cartas de referencia
Hay varios tipos de cartas de referencia, según el uso que se les quiera dar.

Cartas de referencia académicas: provienen de maestros, profesores, orientadores o de alguien que te conozca en el ámbito de la educación.
Cartas de recomendación profesional: las escriben antiguos empleadores, colegas, clientes, proveedores o cualquier otra persona relacionada con su empleo actual o pasado.

https://lopuedohacer.net/laboral/certificado-de-honorabilidad/